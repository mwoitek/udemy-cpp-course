#ifndef WARPDRIVEOVERHEATING_H
#define WARPDRIVEOVERHEATING_H

#include <stdexcept>

using std::overflow_error;

class WarpDriveOverheating : public overflow_error {
 public:
  WarpDriveOverheating()
      : overflow_error("Warp drive has exceeded safe temperature tolerance") {}
};

#endif  // SECTION_7_WARP_DRIVE_WARPDRIVEOVERHEATING_H_

#include <iostream>

#include "WarpDriveOverheating.h"

using std::cout;
using std::endl;

void warpTest(int temperature);

int main() {
  cout << endl << "Testing" << endl << endl;

  int temperature = 50;
  for (int i = 0; i < 4; i++) {
    temperature += 10;
    cout << "Temperature: " << temperature << endl;

    try {
      warpTest(temperature);
    } catch (const WarpDriveOverheating& e) {
      cout << e.what() << endl << endl;
    }
  }

  return 0;
}

void warpTest(int temperature) {
  if (temperature <= 80) {
    cout << "Temperature is within tolerance" << endl << endl;
  } else {
    throw WarpDriveOverheating();
  }
}

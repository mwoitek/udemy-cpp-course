#include <array>
#include <iostream>
#include <stdexcept>

using std::array;
using std::cout;
using std::endl;
using std::out_of_range;

char getLetterGrade(int percent);

int main() {
  array<int, 12> percents = {-1, 0, 59, 60, 69, 70, 79, 80, 89, 90, 100, 101};
  char grade;

  cout << endl;
  for (int percent : percents) {
    cout << "Percent: " << percent << endl;
    try {
      grade = getLetterGrade(percent);
      cout << "Grade: " << grade << endl << endl;
    } catch (const out_of_range& e) {
      cout << e.what() << endl << endl;
    }
  }

  return 0;
}

char getLetterGrade(int percent) {
  if ((percent >= 0) && (percent <= 59)) {
    return 'F';
  } else if ((percent >= 60) && (percent <= 69)) {
    return 'D';
  } else if ((percent >= 70) && (percent <= 79)) {
    return 'C';
  } else if ((percent >= 80) && (percent <= 89)) {
    return 'B';
  } else if ((percent >= 90) && (percent <= 100)) {
    return 'A';
  } else {
    throw out_of_range("Your percent must be within 0 and 100, inclusive");
  }
}

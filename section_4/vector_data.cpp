#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

int main() {
  int user_input;
  vector<int> int_vector;

  cout << "Enter a non-negative integer to continue or a negative integer to "
          "quit:"
       << endl;
  cin >> user_input;

  while (user_input >= 0) {
    int_vector.push_back(user_input);

    cout << "Enter a non-negative integer to continue or a negative integer to "
            "quit:"
         << endl;
    cin >> user_input;
  }

  if (!int_vector.empty()) {
    cout << "Now, here are the double amounts:" << endl;
  }
  for (unsigned int i = 0; i < int_vector.size(); i++) {
    cout << 2 * int_vector[i] << endl;
  }

  return 0;
}

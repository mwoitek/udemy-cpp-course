#include <array>
#include <iostream>

using std::array;
using std::cin;
using std::cout;
using std::endl;

int main() {
  const unsigned int NUM_INTEGERS = 5;
  array<int, NUM_INTEGERS> int_array{};

  for (unsigned int i = 0; i < NUM_INTEGERS; i++) {
    cout << "Enter an integer:" << endl;
    cin >> int_array[i];
  }

  cout << "Now, here are the double amounts:" << endl;
  for (int num : int_array) {
    cout << 2 * num << endl;
  }

  return 0;
}

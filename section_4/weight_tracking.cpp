#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::stod;
using std::string;
using std::vector;

int main() {
  const unsigned int NUM_PEOPLE = 5;
  const unsigned int INPUT_MAX_LENGTH = 80;

  char input[INPUT_MAX_LENGTH];

  vector<string> names;
  vector<double> weights;

  for (unsigned int i = 0; i < NUM_PEOPLE; i++) {
    cout << "Please enter a person's full name:" << endl;
    cin.getline(input, INPUT_MAX_LENGTH);
    names.push_back(input);

    cout << "Please enter " << input << "'s weight:" << endl;
    cin.getline(input, INPUT_MAX_LENGTH);
    weights.push_back(stod(input));
  }

  cout << endl;

  for (unsigned int i = 0; i < names.size(); i++) {
    cout << names[i] << " weights ";
    cout << setprecision(2) << fixed;
    cout << weights[i] << " lbs." << endl;
  }

  return 0;
}

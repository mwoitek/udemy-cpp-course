#include <cstdlib>
#include <ctime>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  /* Seed for random number generator */
  unsigned int seed = static_cast<unsigned int>(time(nullptr));

  /* Random integer between 1 and 100 */
  int secret_number = rand_r(&seed) % 100 + 1;

  /* Number of guesses */
  int num_guesses = 0;

  bool correct = false;
  int guess;

  while (!correct) {
    cout << "Enter your guess:" << endl;
    cin >> guess;
    num_guesses++;

    /* Invalid guess */
    if ((guess < 1) || (guess > 100)) {
      cout << "You just wasted a guess. Pick an integer between 1 and 100."
           << endl;
      continue;
    }

    /* Correct guess */
    if (guess == secret_number) {
      correct = true;
      continue;
    }

    /* Incorrect guess */
    if (guess > secret_number) {
      cout << "Your guess is too high!" << endl;
    } else {
      cout << "Your guess is too low!" << endl;
    }
  }

  cout << "Congratulations! You guessed the number in " << num_guesses
       << " guesses! Thanks for playing!" << endl;

  return 0;
}

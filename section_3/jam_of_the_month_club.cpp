#include <cctype>
#include <iomanip>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::tolower;

int main() {
  char package;
  cout << "Which package do you own? A, B or C?" << endl;
  cin >> package;
  package = tolower(package);

  double package_price;
  unsigned int num_jams_per_month;
  double additional_price;

  switch (package) {
    case 'a':
      package_price = 8;
      num_jams_per_month = 2;
      additional_price = 5;
      break;
    case 'b':
      package_price = 12;
      num_jams_per_month = 4;
      additional_price = 4;
      break;
    case 'c':
      package_price = 15;
      num_jams_per_month = 6;
      additional_price = 3;
      break;
    default:
      cout << "This is not a valid package!" << endl;
      return 1;
  }

  unsigned int num_jams;
  cout << "How many jams, jellies or preserves did you purchase this month?"
       << endl;
  cin >> num_jams;

  double total_cost = package_price;
  if (num_jams > num_jams_per_month) {
    total_cost += additional_price * (num_jams - num_jams_per_month);
  }

  cout << setprecision(2) << fixed;
  cout << "You owe $" << total_cost << endl;

  return 0;
}

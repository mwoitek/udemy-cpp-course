#include <iostream>
#include <stdexcept>

#include "BankAccount.h"

using std::cout;
using std::endl;
using std::invalid_argument;

int main() {
  BankAccount acc1 = BankAccount("Stan Smith");

  acc1.deposit(1000);
  acc1.withdraw(300);

  acc1.deposit(0);
  acc1.withdraw(900);

  cout << acc1.getOwner() << " has $" << acc1.getBalance() << " in his account"
       << endl;

  BankAccount acc2 = BankAccount("Carter Pewterschmidt", 10000000);

  acc2.deposit(-1);
  acc2.withdraw(-1);
  acc2.withdraw(0);

  cout << acc2.getOwner() << " has $" << acc2.getBalance() << " in his account"
       << endl;

  try {
    BankAccount acc3 = BankAccount("");
  } catch (invalid_argument& e) {
    cout << e.what() << endl;
  }

  try {
    BankAccount acc4 = BankAccount("Jeff Fisher", -10000);
  } catch (invalid_argument& e) {
    cout << e.what() << endl;
  }

  return 0;
}

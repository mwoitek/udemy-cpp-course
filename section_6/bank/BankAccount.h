#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H

#include <string>

using std::string;

class BankAccount {
 private:
  string owner;
  int balance;

 public:
  explicit BankAccount(string owner);
  BankAccount(string owner, int balance);
  void deposit(int amount);
  void withdraw(int amount);
  string getOwner();
  int getBalance();
};

#endif  // SECTION_6_BANK_BANKACCOUNT_H_

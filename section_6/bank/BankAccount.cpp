#include "BankAccount.h"

#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;
using std::invalid_argument;
using std::string;

BankAccount::BankAccount(string owner) {
  if (owner.empty()) {
    throw invalid_argument("owner must be a non-empty string");
  } else {
    this->owner = owner;
    this->balance = 0;
  }
}

BankAccount::BankAccount(string owner, int balance) {
  if (owner.empty()) {
    throw invalid_argument("owner must be a non-empty string");
  } else if (balance <= 0) {
    throw invalid_argument("balance must be a positive integer");
  } else {
    this->owner = owner;
    this->balance = balance;
  }
}

void BankAccount::deposit(int amount) {
  if (amount <= 0) {
    cout << "amount must be a positive integer" << endl;
    cout << "Unable to perform transaction" << endl;
  } else {
    this->balance += amount;
    cout << "$" << amount << " have been deposited into " << this->owner
         << "'s account" << endl;
  }
}

void BankAccount::withdraw(int amount) {
  if (amount <= 0) {
    cout << "amount must be a positive integer" << endl;
    cout << "Unable to perform transaction" << endl;
  } else if (amount > this->balance) {
    cout << "Insufficient funds" << endl;
    cout << "Unable to perform transaction" << endl;
  } else {
    this->balance -= amount;
    cout << "$" << amount << " have been withdrawn from " << this->owner
         << "'s account" << endl;
  }
}

string BankAccount::getOwner() { return this->owner; }

int BankAccount::getBalance() { return this->balance; }

#include <iostream>
#include <stdexcept>

#include "Circle.h"

using std::cout;
using std::endl;
using std::invalid_argument;

int main() {
  Circle c1 = Circle();
  cout << "The area of the circle with radius = " << c1.getRadius() << " is "
       << c1.area() << endl;

  Circle c2 = Circle(0.5);
  cout << "The circumference of the circle with radius = " << c2.getRadius()
       << " is " << c2.circumference() << endl;

  c2.setRadius(10);
  cout << "The area of the circle with radius = " << c2.getRadius() << " is "
       << c2.area() << endl;

  c2.setRadius(-1);

  try {
    Circle c3 = Circle(-10);
  } catch (invalid_argument& e) {
    cout << e.what() << endl;
  }

  return 0;
}

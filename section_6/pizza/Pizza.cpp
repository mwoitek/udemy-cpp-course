#include "Pizza.h"

#include <iostream>

using std::cout;
using std::endl;

Pizza::Pizza(string name, int cost, int diameter) {
  this->name = name;
  this->cost = cost;
  this->diameter = diameter;
  this->toppings.push_back("cheese");
}

void Pizza::addTopping(string topping) { this->toppings.push_back(topping); }

int Pizza::getCost() const { return this->cost; }

void Pizza::printToppings() const {
  cout << "Toppings:" << endl;
  for (string topping : this->toppings) {
    cout << "\t" << topping << endl;
  }
}

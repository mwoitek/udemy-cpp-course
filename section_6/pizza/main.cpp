#include <iostream>

#include "Pizza.h"

using std::cout;
using std::endl;

int main() {
  Pizza pizza1 = Pizza("Cheese", 20, 8);
  cout << "Pizza 1 costs $" << pizza1.getCost() << endl;

  Pizza pizza2 = Pizza("Bacon", 25, 10);
  pizza2.addTopping("bacon");
  pizza2.printToppings();

  return 0;
}

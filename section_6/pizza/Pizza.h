#ifndef PIZZA_H
#define PIZZA_H

#include <string>
#include <vector>

using std::string;
using std::vector;

class Pizza {
 private:
  string name;
  int cost;
  int diameter;
  vector<string> toppings;

 public:
  Pizza(string name, int cost, int diameter);
  void addTopping(string topping);
  int getCost() const;
  void printToppings() const;
};

#endif  // SECTION_6_PIZZA_PIZZA_H_

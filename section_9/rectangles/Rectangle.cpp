#include "Rectangle.h"

Rectangle::Rectangle() {
  this->length = 1;
  this->width = 1;
}

Rectangle::Rectangle(double length, double width) {
  this->length = length;
  this->width = width;
}

double Rectangle::getLength() const { return this->length; }

double Rectangle::getWidth() const { return this->width; }

void Rectangle::setLength(double length) { this->length = length; }

void Rectangle::setWidth(double width) { this->width = width; }

double Rectangle::area() const { return this->length * this->width; }

double Rectangle::perimeter() const { return 2 * (this->length + this->width); }

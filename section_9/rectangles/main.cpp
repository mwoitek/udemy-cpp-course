#include <fstream>
#include <iomanip>
#include <vector>

#include "Rectangle.h"

using std::endl;
using std::ifstream;
using std::ofstream;
using std::setw;
using std::vector;

void loadVector(vector<Rectangle*>& rectanglePtrs);
void writeOutputFile(vector<Rectangle*>& rectanglePtrs);
void releaseMemory(vector<Rectangle*>& rectanglePtrs);

int main() {
  vector<Rectangle*> rectanglePtrs;

  loadVector(rectanglePtrs);
  writeOutputFile(rectanglePtrs);
  releaseMemory(rectanglePtrs);

  return 0;
}

void loadVector(vector<Rectangle*>& rectanglePtrs) {
  ifstream inFile;
  inFile.open("rectangles.txt");

  double length;
  double width;
  while (!inFile.eof()) {
    inFile >> length >> width;
    if (!inFile.fail()) {
      rectanglePtrs.push_back(new Rectangle(length, width));
    }
  }

  inFile.close();
}

void writeOutputFile(vector<Rectangle*>& rectanglePtrs) {
  ofstream outFile;
  outFile.open("areas_perimeters.txt");

  outFile << setw(10) << "Area";
  outFile << setw(10) << "Perimeter" << endl;

  for (Rectangle* rectanglePtr : rectanglePtrs) {
    outFile << setw(10) << rectanglePtr->area();
    outFile << setw(10) << rectanglePtr->perimeter() << endl;
  }

  outFile.close();
}

void releaseMemory(vector<Rectangle*>& rectanglePtrs) {
  for (Rectangle* rectanglePtr : rectanglePtrs) {
    delete rectanglePtr;
  }

  rectanglePtrs.clear();
}

#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>

using std::cout;
using std::endl;
using std::getline;
using std::ifstream;
using std::map;
using std::pair;
using std::setw;
using std::string;

int main() {
  ifstream inFile;
  inFile.open("shopping.txt");

  if (!inFile) {
    cout << "Could not read file" << endl;
    return 1;
  }

  map<string, int> frequencies;
  string item;
  while (getline(inFile, item)) {
    frequencies[item]++;
  }
  inFile.close();

  cout << setw(8) << "Item";
  cout << " Frequency" << endl;
  for (const pair<const string, int> &p : frequencies) {
    cout << setw(8) << p.first;
    cout << setw(10) << p.second << endl;
  }

  return 0;
}

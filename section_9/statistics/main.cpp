#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>

using std::accumulate;
using std::cout;
using std::endl;
using std::getline;
using std::ifstream;
using std::setprecision;
using std::setw;
using std::stoi;
using std::string;
using std::vector;

bool fileExists(const string& fileName);
void loadValidScores(const string& fileName, vector<int>& scores);
int computeMinScore(const vector<int>& scores);
int computeMaxScore(const vector<int>& scores);
double computeAvgScore(const vector<int>& scores);

int main() {
  const string fileName = "scores.txt";
  if (!fileExists(fileName)) {
    cout << "File " << fileName << " does not exist" << endl;
    return 1;
  }

  vector<int> scores;
  loadValidScores(fileName, scores);

  cout << setw(14) << "Min Score:";
  cout << setw(6) << computeMinScore(scores) << endl;

  cout << setw(14) << "Max Score:";
  cout << setw(6) << computeMaxScore(scores) << endl;

  cout << "Average Score:";
  cout << setw(6) << setprecision(4) << computeAvgScore(scores) << endl;

  return 0;
}

bool fileExists(const string& fileName) {
  ifstream inFile(fileName);
  return inFile.is_open();
}

void loadValidScores(const string& fileName, vector<int>& scores) {
  ifstream inFile;
  inFile.open(fileName);

  string line;
  int score;
  while (getline(inFile, line)) {
    score = stoi(line);
    if ((score >= 0) && (score <= 100)) {
      scores.push_back(score);
    }
  }

  inFile.close();
}

int computeMinScore(const vector<int>& scores) {
  int minScore = 101;

  for (int score : scores) {
    if (score < minScore) {
      minScore = score;
    }
  }

  return minScore;
}

int computeMaxScore(const vector<int>& scores) {
  int maxScore = -1;

  for (int score : scores) {
    if (score > maxScore) {
      maxScore = score;
    }
  }

  return maxScore;
}

double computeAvgScore(const vector<int>& scores) {
  return accumulate(scores.begin(), scores.end(), 0.0) / scores.size();
}

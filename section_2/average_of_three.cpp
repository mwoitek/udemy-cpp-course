#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  double num1;
  cout << "Please enter a real number:" << endl;
  cin >> num1;

  double num2;
  cout << "Now, enter another real number:" << endl;
  cin >> num2;

  double num3;
  cout << "Now, enter the final real number:" << endl;
  cin >> num3;

  double avg = (num1 + num2 + num3) / 3;
  cout << "Average is " << avg << endl;

  return 0;
}

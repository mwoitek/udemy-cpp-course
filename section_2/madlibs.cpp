#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
  string adjective1;
  cout << "Please enter an adjective:" << endl;
  cin >> adjective1;

  string name_girl;
  cout << "Please enter a girl's name:" << endl;
  cin >> name_girl;

  string adjective2;
  cout << "Please enter another adjective:" << endl;
  cin >> adjective2;

  string occupation1;
  cout << "Please enter an occupation:" << endl;
  cin >> occupation1;

  string place;
  cout << "Please enter the name of a place:" << endl;
  cin >> place;

  string clothing;
  cout << "Please enter the name of an item of clothing in plural:" << endl;
  cin >> clothing;

  string hobby;
  cout << "Please enter a hobby:" << endl;
  cin >> hobby;

  string adjective3;
  cout << "Please enter yet another adjective:" << endl;
  cin >> adjective3;

  string occupation2;
  cout << "Please enter another occupation:" << endl;
  cin >> occupation2;

  string name_boy;
  cout << "Please enter a boy's name:" << endl;
  cin >> name_boy;

  string name_man;
  cout << "Please enter a man's name:" << endl;
  cin >> name_man;

  cout << "There once was a " << adjective1 << " girl named " << name_girl
       << " who was a " << adjective2 << " " << occupation1
       << " in the Kingdom of " << place << ". She loved to wear " << clothing
       << " and " << hobby << ". She wanted to marry the " << adjective3 << " "
       << occupation2 << " named " << name_boy << ", but her father, King "
       << name_man << ", forbid her from seeing him." << endl;

  return 0;
}

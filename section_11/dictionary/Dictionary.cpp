#include "Dictionary.h"

#include <iostream>

void Dictionary::addDefinition(const std::string& word,
                               const std::string& definition) {
  dictionary[word] = definition;
}

std::string Dictionary::getDefinition(const std::string& word) {
  if (dictionary.count(word) > 0) {
    return dictionary[word];
  } else {
    return "NOT FOUND";
  }
}

void Dictionary::printAll() {
  std::cout << "Printing all definitions:\n";
  for (const auto& p : dictionary) {
    std::cout << p.first << ": " << p.second << '\n';
  }
}

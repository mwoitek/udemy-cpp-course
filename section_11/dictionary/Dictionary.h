#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <map>
#include <string>

class Dictionary {
 private:
  std::map<std::string, std::string> dictionary;

 public:
  void addDefinition(const std::string& word, const std::string& definition);
  std::string getDefinition(const std::string& word);
  void printAll();
};

#endif  // SECTION_11_DICTIONARY_DICTIONARY_H_

#ifndef INPUTREADER_H
#define INPUTREADER_H

#include <string>

class InputReader {
 private:
  static void normalizeWord(std::string& word);
  static void normalizeDefinition(std::string& definition);

 public:
  static int readOption();
  static std::string readWord();
  static std::string readDefinition(const std::string& word);
};

#endif  // SECTION_11_DICTIONARY_INPUTREADER_H_

#include <iostream>

#include "Dictionary.h"
#include "InputReader.h"

int main() {
  Dictionary dict;

  int option;
  std::string word;
  std::string definition;

  bool quit = false;
  while (!quit) {
    option = InputReader::readOption();

    switch (option) {
      case 1:
        word = InputReader::readWord();
        definition = InputReader::readDefinition(word);
        dict.addDefinition(word, definition);
        break;

      case 2:
        word = InputReader::readWord();
        definition = dict.getDefinition(word);
        std::cout << "Definition for " << word << ":\n";
        std::cout << definition << '\n';
        break;

      case 3:
        dict.printAll();
        break;

      case 0:
        quit = true;
    }
  }

  return 0;
}

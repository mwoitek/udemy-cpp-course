#include "InputReader.h"

#include <boost/algorithm/string.hpp>
#include <iostream>
#include <limits>

void InputReader::normalizeWord(std::string& word) {
  boost::algorithm::to_lower(word);
}

void InputReader::normalizeDefinition(std::string& definition) {
  boost::algorithm::trim(definition);
}

int InputReader::readOption() {
  int option;

  bool validOption = false;
  while (!validOption) {
    std::cout << "Select an option:\n";
    std::cout << "1 - Add a new word and definition\n";
    std::cout << "2 - Get the definition for a word\n";
    std::cout << "3 - Print all definitions\n";
    std::cout << "0 - Exit the program\n";

    if ((std::cin >> option) && (option >= 0) && (option <= 3)) {
      validOption = true;
    } else {
      std::cout << "Please enter a valid option\n";
      std::cin.clear();
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

  return option;
}

std::string InputReader::readWord() {
  std::string word;

  std::cout << "Enter the word:\n";
  std::cin >> word;
  normalizeWord(word);
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  return word;
}

std::string InputReader::readDefinition(const std::string& word) {
  std::string definition;

  std::cout << "Enter the definition for " << word << ":\n";
  std::getline(std::cin, definition);
  normalizeDefinition(definition);

  return definition;
}

#include <array>
#include <iostream>

using std::array;
using std::cout;
using std::endl;

const unsigned int ARRAY_SIZE = 10;

int sum_array(array<int, ARRAY_SIZE> int_array);

int main() {
  array<int, ARRAY_SIZE> test_array;
  for (unsigned int i = 0; i < test_array.size(); i++) {
    test_array[i] = i + 1;
  }

  cout << "The sum of all integers between 1 and " << test_array.size()
       << " is " << sum_array(test_array) << "." << endl;

  return 0;
}

int sum_array(array<int, ARRAY_SIZE> int_array) {
  int sum = 0;
  for (int num : int_array) {
    sum += num;
  }
  return sum;
}

#include <iostream>
#include <limits>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::numeric_limits;
using std::streamsize;
using std::string;
using std::vector;

void print_welcome_message();
bool prompt_yes_or_no(string prompt_message);
bool prompt_play_or_quit(bool first_time = false);
void print_quit_message(bool first_time = false);
void print_players_symbols(const char symbols[3]);
void print_board_row(int row, const int board[3][3], const char symbols[3]);
void print_board(const int board[3][3], const char symbols[3]);
void print_turn_message(int player);
void print_game_end_message(bool somebody_won, int player);

void initialize_board(int board[3][3]);
void initialize_game(int board[3][3], int& turn, bool& somebody_won,
                     int& player);
int get_other_player(int player);
bool is_coordinate_valid(int coordinate);
bool are_coordinates_valid(const vector<int>& coordinates);
bool is_board_position_empty(const vector<int>& coordinates,
                             const int board[3][3]);
void get_valid_coordinates(vector<int>& coordinates, const int board[3][3]);
void update_board(int player, const vector<int>& coordinates, int board[3][3]);

bool check_row(int row, int player, const int board[3][3]);
bool check_col(int col, int player, const int board[3][3]);
bool check_main_diagonal(int player, const int board[3][3]);
bool check_secondary_diagonal(int player, const int board[3][3]);
bool check_rows(int player, const int board[3][3]);
bool check_cols(int player, const int board[3][3]);
bool check_diagonals(int player, const int board[3][3]);
bool has_player_won(int player, const int board[3][3]);
bool has_game_ended(int turn, bool somebody_won);

int main() {
  print_welcome_message();

  bool quit = !prompt_play_or_quit(true);
  if (quit) {
    print_quit_message(true);
    return 0;
  }

  /* TODO Add option to choose symbols */
  char symbols[3]{' ', 'X', 'O'};
  cout << endl << "Great! Let's start!" << endl;
  print_players_symbols(symbols);

  /* Game state variables */
  int board[3][3];
  int turn;
  bool somebody_won;
  int player;

  vector<int> coordinates;

  /* Game loop */
  while (!quit) {
    initialize_game(board, turn, somebody_won, player);

    /* TODO Add option to quit in the middle of the game */
    while (!has_game_ended(turn, somebody_won)) {
      player = get_other_player(player);

      print_board(board, symbols);
      print_turn_message(player);
      get_valid_coordinates(coordinates, board);
      update_board(player, coordinates, board);

      turn++;
      somebody_won = has_player_won(player, board);
    }

    print_board(board, symbols);
    print_game_end_message(somebody_won, player);

    quit = !prompt_play_or_quit();
  }

  print_quit_message();
  return 0;
}

void print_welcome_message() {
  cout << "TIC-TAC-TOE" << endl;
  cout << "-----------" << endl << endl;
  cout << "Welcome, players!" << endl;
}

bool prompt_yes_or_no(string prompt_message) {
  cout << endl << prompt_message << " (Y/N)" << endl;

  char answer;
  bool is_answer_valid = false;

  while (!is_answer_valid) {
    cin >> answer;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    answer = tolower(answer);

    if ((answer == 'y') || (answer == 'n')) {
      is_answer_valid = true;
    } else {
      cout << "Your answer is not valid. Please choose Y or N." << endl;
    }
  }

  return answer == 'y';
}

bool prompt_play_or_quit(bool first_time) {
  string again = first_time ? "" : " again";
  string prompt_message = "Do you want to play" + again + "?";

  return prompt_yes_or_no(prompt_message);
}

void print_quit_message(bool first_time) {
  if (first_time) {
    cout << endl << "Too bad. Later give it a try!" << endl;
  } else {
    cout << endl << "Thanks for playing!" << endl;
  }
}

void print_players_symbols(const char symbols[3]) {
  cout << endl;
  for (int i = 1; i < 3; i++) {
    cout << "Player " << i << " will use " << symbols[i] << "'s." << endl;
  }
}

void print_board_row(int row, const int board[3][3], const char symbols[3]) {
  char symbols_to_print[3];
  for (int col = 0; col < 3; col++) {
    symbols_to_print[col] = symbols[board[row][col]];
  }

  cout << " " << symbols_to_print[0] << " | ";
  cout << symbols_to_print[1] << " | ";
  cout << symbols_to_print[2] << " " << endl;

  if (row < 2) {
    cout << "-----------" << endl;
  }
}

void print_board(const int board[3][3], const char symbols[3]) {
  cout << endl << "Here's the board:" << endl << endl;
  for (int row = 0; row < 3; row++) {
    print_board_row(row, board, symbols);
  }
}

void print_turn_message(int player) {
  cout << endl
       << "Player " << player
       << ", it's your turn. Where do you want to put a mark?" << endl;
  cout
      << "Enter the coordinates of the board position separated by whitespaces:"
      << endl;
}

void print_game_end_message(bool somebody_won, int player) {
  if (somebody_won) {
    cout << endl
         << "Congratulations, player " << player << "! You have won!" << endl;
    cout << "Sorry, player " << get_other_player(player)
         << ". Better luck next time!" << endl;
  } else {
    cout << endl
         << "Congratulations to both players! The game has ended in a draw!"
         << endl;
  }
}

void initialize_board(int board[3][3]) {
  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      board[row][col] = 0;
    }
  }
}

void initialize_game(int board[3][3], int& turn, bool& somebody_won,
                     int& player) {
  initialize_board(board);

  turn = 0;
  somebody_won = false;
  player = 2;
}

int get_other_player(int player) { return player == 1 ? 2 : 1; }

bool is_coordinate_valid(int coordinate) {
  return (coordinate > 0) && (coordinate < 4);
}

bool are_coordinates_valid(const vector<int>& coordinates) {
  return is_coordinate_valid(coordinates[0]) &&
         is_coordinate_valid(coordinates[1]);
}

bool is_board_position_empty(const vector<int>& coordinates,
                             const int board[3][3]) {
  return board[coordinates[0] - 1][coordinates[1] - 1] == 0;
}

void get_valid_coordinates(vector<int>& coordinates, const int board[3][3]) {
  int x;
  int y;

  bool valid = false;
  while (!valid) {
    coordinates.clear();

    if (cin >> x >> y) {
      coordinates.push_back(x);
      coordinates.push_back(y);
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
    } else {
      cout << "Invalid input! Please enter 2 integers between 1 and 3:" << endl;
      cin.clear();
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      continue;
    }

    if (!are_coordinates_valid(coordinates)) {
      cout << "Invalid coordinates! Please enter 2 integers between 1 and 3:"
           << endl;
    } else if (!is_board_position_empty(coordinates, board)) {
      cout << "Position already filled! Please choose an empty one:" << endl;
    } else {
      valid = true;
    }
  }
}

void update_board(int player, const vector<int>& coordinates, int board[3][3]) {
  board[coordinates[0] - 1][coordinates[1] - 1] = player;
}

bool check_row(int row, int player, const int board[3][3]) {
  bool all_equal = true;
  for (int col = 0; col < 3; col++) {
    if (board[row][col] != player) {
      all_equal = false;
      break;
    }
  }
  return all_equal;
}

bool check_col(int col, int player, const int board[3][3]) {
  bool all_equal = true;
  for (int row = 0; row < 3; row++) {
    if (board[row][col] != player) {
      all_equal = false;
      break;
    }
  }
  return all_equal;
}

bool check_main_diagonal(int player, const int board[3][3]) {
  bool all_equal = true;
  for (int i = 0; i < 3; i++) {
    if (board[i][i] != player) {
      all_equal = false;
      break;
    }
  }
  return all_equal;
}

bool check_secondary_diagonal(int player, const int board[3][3]) {
  bool all_equal = true;

  int row = 0;
  int col = 2;

  while (row < 3) {
    if (board[row][col] != player) {
      all_equal = false;
      break;
    }

    row++;
    col--;
  }

  return all_equal;
}

bool check_rows(int player, const int board[3][3]) {
  bool won = false;
  int row = 0;

  while (!won && (row < 3)) {
    won = check_row(row, player, board);
    row++;
  }

  return won;
}

bool check_cols(int player, const int board[3][3]) {
  bool won = false;
  int col = 0;

  while (!won && (col < 3)) {
    won = check_col(col, player, board);
    col++;
  }

  return won;
}

bool check_diagonals(int player, const int board[3][3]) {
  return check_main_diagonal(player, board) ||
         check_secondary_diagonal(player, board);
}

bool has_player_won(int player, const int board[3][3]) {
  return check_rows(player, board) || check_cols(player, board) ||
         check_diagonals(player, board);
}

bool has_game_ended(int turn, bool somebody_won) {
  return somebody_won || (turn == 9);
}

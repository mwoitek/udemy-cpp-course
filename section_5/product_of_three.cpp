#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int multiply(int num1, int num2, int num3);

int main() {
  int num1;
  cout << "Enter the first integer:" << endl;
  cin >> num1;

  int num2;
  cout << "Enter the second integer:" << endl;
  cin >> num2;

  int num3;
  cout << "Enter the third integer:" << endl;
  cin >> num3;

  cout << "The product of these integers is " << multiply(num1, num2, num3)
       << "." << endl;

  return 0;
}

int multiply(int num1, int num2, int num3) { return num1 * num2 * num3; }

#include <iostream>

using std::cout;
using std::endl;

int sum_array(int int_array[], unsigned int array_size);

int main() {
  const unsigned int TEST_SIZE = 100;

  int test_array[TEST_SIZE];
  for (unsigned int i = 0; i < TEST_SIZE; i++) {
    test_array[i] = i + 1;
  }

  cout << "The sum of all integers between 1 and " << TEST_SIZE << " is "
       << sum_array(test_array, TEST_SIZE) << "." << endl;

  return 0;
}

int sum_array(int int_array[], unsigned int array_size) {
  int sum = 0;
  for (unsigned int i = 0; i < array_size; i++) {
    sum += int_array[i];
  }
  return sum;
}

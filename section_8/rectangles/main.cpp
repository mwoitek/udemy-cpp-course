#include <iomanip>
#include <iostream>

#include "Rectangle.h"

using std::cout;
using std::endl;
using std::setw;

int main() {
  Rectangle* rectanglePtrs[3];

  rectanglePtrs[0] = new Rectangle();
  rectanglePtrs[1] = new Rectangle(2, 3);
  rectanglePtrs[2] = new Rectangle(5, 7);

  cout << endl;
  for (int i = 0; i < 3; i++) {
    cout << "Rectangle " << i + 1 << endl << endl;

    cout << setw(10) << "Area:";
    cout << setw(4) << rectanglePtrs[i]->area() << endl;

    cout << "Perimeter:";
    cout << setw(4) << rectanglePtrs[i]->perimeter() << endl << endl;
  }

  for (int i = 0; i < 3; i++) {
    delete rectanglePtrs[i];
    rectanglePtrs[i] = nullptr;
  }

  return 0;
}

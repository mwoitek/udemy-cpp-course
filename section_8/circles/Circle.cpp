#include "Circle.h"

#include <cmath>
#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;
using std::invalid_argument;

Circle::Circle() { this->radius = 1; }

Circle::Circle(double radius) {
  if (radius <= 0) {
    throw invalid_argument("radius must be a positive real number");
  } else {
    this->radius = radius;
  }
}

double Circle::getRadius() { return this->radius; }

void Circle::setRadius(double radius) {
  if (radius <= 0) {
    cout << "radius must be a positive real number" << endl;
    cout << "unable to change the circle radius" << endl;
  } else {
    this->radius = radius;
  }
}

double Circle::circumference() { return 2 * M_PI * this->radius; }

double Circle::area() { return M_PI * pow(this->radius, 2); }

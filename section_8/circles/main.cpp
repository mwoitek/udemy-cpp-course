#include <iostream>
#include <limits>

#include "Circle.h"

int getNumberCircles();
double getRadius();
void loadCirclePtrsArray(Circle** circlePtrs, int numberCircles);
void printCircumferencesAreas(Circle** circlePtrs, int numberCircles);
void releaseMemory(Circle** circlePtrs, int numberCircles);

int main() {
  int numberCircles = getNumberCircles();
  Circle** circlePtrs = new Circle*[numberCircles];

  loadCirclePtrsArray(circlePtrs, numberCircles);
  printCircumferencesAreas(circlePtrs, numberCircles);
  releaseMemory(circlePtrs, numberCircles);

  return 0;
}

int getNumberCircles() {
  int numberCircles;
  bool validInput = false;

  while (!validInput) {
    std::cout << "How many circles do you want to create?" << std::endl;

    if ((std::cin >> numberCircles) && (numberCircles > 0)) {
      validInput = true;
    } else {
      std::cout << "Please enter a positive integer" << std::endl;
      std::cin.clear();
    }

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

  return numberCircles;
}

double getRadius() {
  double radius;
  bool validInput = false;

  while (!validInput) {
    std::cout << "Enter the circle radius:" << std::endl;

    if ((std::cin >> radius) && (radius > 0)) {
      validInput = true;
    } else {
      std::cout << "Please enter a positive real number" << std::endl;
      std::cin.clear();
    }

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

  return radius;
}

void loadCirclePtrsArray(Circle** circlePtrs, int numberCircles) {
  double radius;

  for (int i = 0; i < numberCircles; i++) {
    std::cout << "Circle " << i + 1 << std::endl;

    radius = getRadius();
    circlePtrs[i] = new Circle(radius);
  }
}

void printCircumferencesAreas(Circle** circlePtrs, int numberCircles) {
  for (int i = 0; i < numberCircles; i++) {
    std::cout << "Circle " << i + 1 << std::endl;
    std::cout << "Circumference: " << circlePtrs[i]->circumference()
              << std::endl;
    std::cout << "Area: " << circlePtrs[i]->area() << std::endl;
  }
}

void releaseMemory(Circle** circlePtrs, int numberCircles) {
  for (int i = 0; i < numberCircles; i++) {
    delete circlePtrs[i];
  }

  delete[] circlePtrs;
}

#ifndef CIRCLE_H
#define CIRCLE_H

class Circle {
 private:
  double radius;

 public:
  Circle();
  explicit Circle(double radius);
  double getRadius();
  void setRadius(double radius);
  double circumference();
  double area();
};

#endif  // SECTION_8_CIRCLES_CIRCLE_H_

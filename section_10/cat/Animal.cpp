#include "Animal.h"

Animal::Animal(std::string name, double weight) {
  this->name = name;
  this->weight = weight;
}

std::string Animal::getName() const { return name; }

double Animal::getWeight() const { return weight; }

void Animal::setName(std::string name) { this->name = name; }

void Animal::setWeight(double weight) { this->weight = weight; }

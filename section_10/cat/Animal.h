#ifndef ANIMAL_H
#define ANIMAL_H

#include <string>

class Animal {
 private:
  std::string name;
  double weight;

 public:
  Animal(std::string name, double weight);
  std::string getName() const;
  double getWeight() const;
  void setName(std::string name);
  void setWeight(double weight);
  virtual ~Animal() = default;
  virtual std::string makeNoise() const = 0;
  virtual std::string eat() const = 0;
};

#endif  // SECTION_10_CAT_ANIMAL_H_

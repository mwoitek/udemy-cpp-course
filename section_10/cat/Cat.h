#ifndef CAT_H
#define CAT_H

#include <iostream>
#include <string>

#include "Animal.h"

class Cat : public Animal {
 public:
  Cat(std::string name, double weight) : Animal(name, weight) {}

  std::string makeNoise() const { return "Meow!"; }

  std::string eat() const { return "Tasty kitty food!"; }

  void chaseMouse() const { std::cout << "I'm chasing a mouse!" << std::endl; }
};

#endif  // SECTION_10_CAT_CAT_H_

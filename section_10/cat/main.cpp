#include <iostream>

#include "Cat.h"

int main() {
  Animal* myCat = new Cat("Garfield", 50);

  std::cout << "makeNoise(): " << myCat->makeNoise() << std::endl;
  std::cout << "eat(): " << myCat->eat() << std::endl;

  delete myCat;
  myCat = nullptr;

  Cat anotherCat("Sylvester", 40);

  std::cout << "chaseMouse(): ";
  anotherCat.chaseMouse();

  return 0;
}
